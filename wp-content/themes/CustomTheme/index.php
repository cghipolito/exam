<?php
    
    if($_POST){
        $payment_method =  $_POST['payment_method'];
        $fname = $_POST['first_name'];
        $lname = $_POST['last_name'];
        $demail = $_POST['donator_email'];
        $phone = $_POST['phone'];
        $amount = $_POST['amount'];

        $donation = array(
          'post_title'    => false,
          'post_content'  => false,
          'post_status'   => 'publish',
          'post_type'=>'donations', 
        );
         
        $post_id = wp_insert_post( $donation );
        add_post_meta($post_id, 'payment_method', $payment_method);
        add_post_meta($post_id, 'first_name', $fname);
        add_post_meta($post_id, 'last_name', $lname);
        add_post_meta($post_id, 'email', $demail);
        add_post_meta($post_id, 'phone', $phone);
        add_post_meta($post_id, 'donation_amount', $amount);
    }
    
    global $wpdb;
    get_header();

    $sum = $wpdb->get_results("SELECT SUM(meta_value) FROM $wpdb->postmeta as postmeta 
                                INNER JOIN $wpdb->posts as posts 
                                ON postmeta.post_id = posts.ID 
                                WHERE postmeta.meta_key = 'donation_amount' 
                                AND posts.post_status = 'publish' 
                                AND posts.post_type = 'donations' ");

    foreach($sum[0] as $sum){
        $total = $sum;
    }
?>
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center pt-5"><?= get_the_title() ?></h1>
                <p class="text-center"><?= get_the_content() ?></p>

                <div class="form-methods text-center mt-4">
                    <span class="text-center">Lorem Ipsum</span>
                    <h2 class="current-amount">$ <span class="count" data-total="<?= $total ?>" ><?= $total ?></span></h2>
                    <span>of $4 million raised</span>
                    
					<div class="progress" role="progressbar" data-goal="60" aria-valuemin="0" aria-valuemax="100">
						<div class="progress__bar" style="width: 50%"></div>
					</div>

                    <div class="input-group mb-3" id="input-number">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input type="number" class="form-control" id="amount-input" aria-label="Amount (to the nearest dollar)" placeholder="10,000.00" >
                    </div>
                    <div class="lorem-comment">
                        <?= wpautop(get_field('form_paragraph'));  ?>
                    </div>
                    <div class="select-payment-method mt-5">
                        <form action="" method="POST">  
                            <h3 class="text-left">Select payment method</h3>
                            <hr>
                            <div class="radio-checkbox">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="payment_method" type="radio" id="inlineCheckbox1" value="Paypal" checked="checked" >
                                    <label class="form-check-label" for="inlineCheckbox1">Paypal</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="payment_method" type="radio" id="inlineCheckbox2" value="Offline">
                                    <label class="form-check-label" for="inlineCheckbox2">Offline donation</label>
                                </div>
                            </div>
                            <h3 class="text-left mt-4">Personal info</h3>
                            <div class="personal-form mt-4">
                                <div class="row" id="personal-info">
                                    <div class="col-md-6">
                                        <input type="text" id="first_name" name="first_name" placeholder="First name*" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="last_name" name="last_name" placeholder="Last name*" required>
                                    </div> 
                                    <div class="col-md-6">
                                        <input type="email" id="donator_email" name="donator_email" placeholder="Email*" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="tel" id="phone" name="phone_number" placeholder="Phone*" required>
                                    </div>                           
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div id="input-donation">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Donation total:</span>
                                            </div>
                                            <input type="text" class="form-control" id="amount" name="amount" aria-label="Amount (to the nearest dollar)" placeholder="10,000.00">
                                            <button class="btn btn-secondary btn-lg" id="donate-now">DONATE NOW</button>                                          
                                        </div>                               
                                    </div>                                  
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="second-section" class="mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-5 child-img">
                <img src="<?= get_field('bottom_image') ?>" alt="Child" >
            </div>
            <div class="col-md-7 my-auto" class="sub-ctn">
                <div class="bottom-desc">
                    <?= wpautop(get_field('bottom_paragraph') ) ?>
                </div>
            </div>
        </div>
    </div>
</section>



<?php 
get_footer();
 
?>