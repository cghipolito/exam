<?php
function enqueue(){
    wp_enqueue_style('custom', get_template_directory_uri(). '/css/custom.css');	
    wp_enqueue_style('style', get_template_directory_uri(). '/style.css');
	wp_enqueue_style('progresscss', get_template_directory_uri(). '/css/asProgress.css');

    wp_enqueue_style('bootsrapmin','https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');
	  wp_enqueue_script('bootsrapjs','https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js');
    wp_enqueue_script('bootsrapminjs','https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js');

    wp_enqueue_style('font-awesome','https://use.fontawesome.com/releases/v5.13.0/css/all.css');

    wp_enqueue_style('lato','https://fonts.googleapis.com/css?family=Lato');
    wp_enqueue_style('Poppins','https://fonts.googleapis.com/css?family=Poppins');
    
	wp_enqueue_script('asProgress',get_stylesheet_directory_uri() .'/js/jquery-asProgress.js');
    wp_enqueue_script('main',get_stylesheet_directory_uri() .'/js/main.js');
	
	wp_enqueue_style('progressbarcss','https://cdnjs.cloudflare.com/ajax/libs/jquery.percentageloader/0.1.0/jquery.percentageloader.css');
	
	
}
add_action('wp_enqueue_scripts', 'enqueue');

$args = array(
    'name'          => __( 'Main Sidebar', 'mytheme' ),
    'id'            => 'sidebar-1',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>'
);
register_sidebar( $args );

add_action( 'init', 'register_my_menu' );
function register_my_menu() {
    register_nav_menu( 'primary', 'Primary Menu' );
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

function create_posttype() {
 
    register_post_type( 'donations',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Donations' ),
                'singular_name' => __( 'Donations' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'donations'),
            'show_in_rest' => true,
            'supports' => array('thumbnail')
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

add_filter('manage_posts_columns', 'my_columns');
function my_columns($columns) {
	$columns['Payment-Method'] = 'Payment Method';	
	$columns['First-Name'] = 'First Name';
	$columns['Last-Name'] = 'Last Name';
	$columns['Donations-Amount'] = 'Donation Amount';
	$columns['Email'] = 'Email';
	return $columns;
}

add_action('manage_posts_custom_column', 'my_show_columns');
function my_show_columns($name) {
	unset($columns['title']);
	switch ($name) {
		case 'Payment-Method':
		$payment_method = get_post_meta(get_the_ID(), "payment_method", true);
		$payment_method = $payment_method;
		echo $payment_method;
		break;

		case 'First-Name':
		$first_name = get_post_meta(get_the_ID(), "first_name", true);
		$first_names = $first_name;
		echo $first_names;
		break;
		
		case 'Last-Name':
		$last_name = get_post_meta(get_the_ID(), "last_name", true);
		$last_names = $last_name;
		echo $last_names;
		break;

		case 'Donations-Amount':
		$value = get_post_meta(get_the_ID(), "donation_amount", true);
		$feat = $value;
		echo $feat;
		break;

		case 'Email':
		$value = get_post_meta(get_the_ID(), "email", true);
		$email = $value;
		echo $email;		
		
	}
}

?>