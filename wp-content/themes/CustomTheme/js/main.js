$(document).ready(function(){
	$('.count').each(function () {
		$(this).prop('Counter',0).animate({
				Counter: $(this).text()
		}, {
			duration: 4000,
			easing: 'swing',
			step: function (now) {
					$(this).text(Math.ceil(now));
			}
		});
	});

})
 jQuery(function($) {
 	var currentProgress = parseInt($('.count').attr('data-total'));
 	var totalAmount = 4000000;
	$('.progress').asProgress({
        'namespace': 'progress'
    });

    loadProgress =  (currentProgress / totalAmount) * 100
    $('.progress').asProgress('go', loadProgress+"%");
    
	//amount change of progress bar
	$("#amount-input").on("keyup", function(){
		var amountChange = ((parseInt(this.value) + currentProgress) / totalAmount) * 100;
		$('#amount').val(this.value);
		$('.progress').asProgress('go', amountChange.toFixed(2) + "%");
	});

	$("#amount").on("keyup", function(){
		var amountChange = ((parseInt(this.value) + currentProgress) / totalAmount) * 100;
		$('#amount-input').val(this.value);
		$('.progress').asProgress('go', amountChange.toFixed(2) + "%");
	});	
});